# OS in a container

> lol, k8s witchcraft goes brrrr

> "★★★★★ 5/5, it takes a great understanding k8s, docker, system engineering and GitLab CI to make this black magic." Myself, 2023

## Usage

* Cronjob is used to leverage the pod lifecycle
    * Once the command exits, I want the pod to be deleted/ended, such that
        * the `latest` tag is really "latest" (pulling new image if needed)
        * to test `setup.sh` is working as expected
    * Deployment, DaemonSets, ReplicaSets, ReplicationController bound by pod restartPolicy will not respawn new pod, see [this SO](https://stackoverflow.com/questions/63400156/configure-restart-policy-of-pod-from-deployment-config)
* `/root` is mounted as a persistence volume
    * root is the default user...figure it out why this is a "black magic"
* supervisord is used as entrypoint
    * systemd + docker/k8s is a absolute horror show, avoid at all cost
* `setup.sh` is executed when pod starts, which
    * fetch supervisord config and override local config and reload it
        * maybe supervisord should include a true local persistent config, not sure if needed
    * allow user to define more crazy stuff
* ssh is enabled by default, now you should know why it is "OS in a container"