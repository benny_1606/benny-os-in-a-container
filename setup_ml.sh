#!/bin/bash

# Cause who cares about eRrOr hANdlinG in *BASH*
set -ex

mkdir -p /root/.persistence

if [ ! -f "/root/.persistence/ssh_host_ed25519_key" ]; then
    ssh-keygen -q -N "" -t ed25519 -f /root/.persistence/ssh_host_ed25519_key
fi

if [ -f "/root/.persistence/authorized_keys" ]; then
    if ! grep -q "stalin@Stalingrad" "/root/.persistence/authorized_keys"; then
        echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS8ZvS25gHjztcQ3Ivdu3Jk9KshrzxJmMKvbwZ88Ab6XW0vvCsxkkxnKbiwT8NEB8+7g+ALIz+/3s7RyFREO1QWHlZri/kXqYc7IJp15OH3H7dXXgiKUfa1FLB0fTJaoebosOINQsuRLyJ3N2cHIUjy1YDIf3nxbG5G9PkB11/ZvwndUrQEUizfN+JZbR3BueJlDM/+EZCGvvI3GQw+Wqw3N4U8GRqIcFDlWGOpx1gCs9yHkJeaXICYuOjMVF6dozncaewRxMsRSIZIY4VtJeBqYyPx9f+FVPq+JNZiPTcvNouAaYR/aar1XqPGBXf+phnqO+Cw+Xikn061V8xD3IvuZSrAirKhSdiujufV3pzoTwpztFRGCpqPTbRrs57sRugc62dEzCePlnZJkup06IM5S6UGAnRI/+Uw5nJrI7UYWQLCQIdnnEywvzoEJ0LfyRhC4ls92nB/8eO+7/Yht/eFi4tFf6J3fwTZUUPFWpw8w77bf2fHV44CCHvGH5Yrb6ekoHuCwBXuHmQZVcp2frkPmq9p3Z6HOyM2zAln424gRHMZfFabIO9QEfN7tKqkHYOWCuVJWHLO5kTpr+4aUpHg7ws9IxSmT2wM3oDzq9gLlrnV6ETvfi4Fa6mWxdRDhmVc0a/tKzsEmE/KJ1PwdSda0B+nVtW1yRNK/lZ3nCWmQ== stalin@Stalingrad" >> /root/.persistence/authorized_keys
    fi
else
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS8ZvS25gHjztcQ3Ivdu3Jk9KshrzxJmMKvbwZ88Ab6XW0vvCsxkkxnKbiwT8NEB8+7g+ALIz+/3s7RyFREO1QWHlZri/kXqYc7IJp15OH3H7dXXgiKUfa1FLB0fTJaoebosOINQsuRLyJ3N2cHIUjy1YDIf3nxbG5G9PkB11/ZvwndUrQEUizfN+JZbR3BueJlDM/+EZCGvvI3GQw+Wqw3N4U8GRqIcFDlWGOpx1gCs9yHkJeaXICYuOjMVF6dozncaewRxMsRSIZIY4VtJeBqYyPx9f+FVPq+JNZiPTcvNouAaYR/aar1XqPGBXf+phnqO+Cw+Xikn061V8xD3IvuZSrAirKhSdiujufV3pzoTwpztFRGCpqPTbRrs57sRugc62dEzCePlnZJkup06IM5S6UGAnRI/+Uw5nJrI7UYWQLCQIdnnEywvzoEJ0LfyRhC4ls92nB/8eO+7/Yht/eFi4tFf6J3fwTZUUPFWpw8w77bf2fHV44CCHvGH5Yrb6ekoHuCwBXuHmQZVcp2frkPmq9p3Z6HOyM2zAln424gRHMZfFabIO9QEfN7tKqkHYOWCuVJWHLO5kTpr+4aUpHg7ws9IxSmT2wM3oDzq9gLlrnV6ETvfi4Fa6mWxdRDhmVc0a/tKzsEmE/KJ1PwdSda0B+nVtW1yRNK/lZ3nCWmQ== stalin@Stalingrad" >> /root/.persistence/authorized_keys
fi

mkdir /run/sshd
cat >/etc/ssh/sshd_config.d/00-benny.conf <<EOL
UseDNS no
HostKey /root/.persistence/ssh_host_ed25519_key
AuthorizedKeysFile /root/.persistence/authorized_keys

PasswordAuthentication no
PermitRootLogin prohibit-password

ciphers chacha20-poly1305@openssh.com
kexalgorithms curve25519-sha256,curve25519-sha256@libssh.org
EOL

byobu-enable

rm -f /root/.persistence/supervisord.conf*

wget -O /root/.persistence/supervisord.conf https://gitlab.com/benny_1606/benny-os-in-a-container/-/raw/main/supervisord_ml.conf

cat /etc/supervisord_init.conf

cat /root/.persistence/supervisord.conf

ls /root/.persistence

ls /root

mkdir -p /root/scripts
run-parts --test /root/scripts
run-parts /root/scripts

