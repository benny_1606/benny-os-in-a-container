FROM debian:trixie-backports

ENV container docker

RUN apt-get update && apt-get install -y byobu openssh-server neovim fish wget supervisor python3 python3-pip git htop && apt-get clean all

RUN chsh -s /usr/bin/fish root
RUN mkdir /run/sshd

WORKDIR /root

ADD supervisord_init.conf /etc/supervisord_init.conf

CMD [ "/usr/bin/supervisord", "-c", "/etc/supervisord_init.conf", "-n" ]
